#include <ros/ros.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <std_msgs/Float32.h>
#include <cmath>
#include <path_planner.h>
#include "cylinder.h"
#include "plane.h"
#include <unistd.h>
#include <iostream>
#include <fstream>

PathPlanner* pathPlanner;

void load_file();

void getParams(ros::NodeHandle &nh);

void pose_cb (const geometry_msgs::PoseStamped pose_msg);

int main (int argc, char** argv) {


    //Initializing the path planner with the two surfaces which define the path
    Surface_function* f1 = new Cylinder(2,2,0,0);
//    Surface_function* f1 = new Plane(1,0,0,0);
    Surface_function* f2 = new Plane (0,0,1,-1.4);

//Surface_function* f1 = new Plane(1,0,0,0);
//Surface_function* f2 = new Plane (0,0,1,-1.4);
    pathPlanner = new PathPlanner(f1,f2);

    // Initialize ROS
    ros::init (argc, argv, "trajectory_planner_node");
    ros::NodeHandle nh("~");
    std::ofstream fs2("data_test.txt", std::ofstream::out | std::ofstream::trunc);

    if(fs2.is_open()){
	fs2 << "time \t";
        fs2 << "x \t";
        fs2 << "y \t";
        fs2 << "z \n";
	}

    ros::Subscriber poseSub = nh.subscribe<geometry_msgs::PoseStamped>("/firefly/odometry_sensor1/pose",1,pose_cb);

    //Publishing topics
    ros::Publisher pub = nh.advertise<trajectory_msgs::MultiDOFJointTrajectory>("/firefly/command/trajectory",1);
    ros::Publisher pub_plot_x = nh.advertise<std_msgs::Float32>("/xplot",1);
    ros::Publisher pub_plot_y = nh.advertise<std_msgs::Float32>("/yplot",1);
    ros::Publisher pub_plot_z = nh.advertise<std_msgs::Float32>("/zplot",1);

    getParams(nh);

    //Declaring messages
    trajectory_msgs::MultiDOFJointTrajectory velocityMsg;
    geometry_msgs::PoseStamped robotPose;
    Eigen::Vector3d velocityVector;
    std_msgs::Float32 x_plot;
    std_msgs::Float32 y_plot;
    std_msgs::Float32 z_plot;


    load_file();

    velocityMsg.header.frame_id = "world";
    sleep(1);
    ros::spinOnce();
    std::cout << "\n**********************************************" << std::endl;
    std::cout << "* Firefly is following the trajectory!       *" << std::endl;
    std::cout << "* press K to load new obstacles / parameters *" << std::endl;
    std::cout << "**********************************************" << std::endl;
    fcntl(0, F_SETFL, O_NONBLOCK);
    char x=47;
    int w;
    sleep(1);

   
    // Spin
    ros::Rate rate(100);

    while (ros::ok()){
         scanf("%c", &x);
         w=int(x)-48;
         fcntl(0, F_SETFL, O_NONBLOCK);

	if (x=='k')
	{
        load_file();
   	std::cout << "\n**********************************************" << std::endl;
    	std::cout << "* New parameters loaded		           *" << std::endl;
    	std::cout << "* press K to load new obstacles / parameters *" << std::endl;
    	std::cout << "**********************************************" << std::endl;
   	fcntl(0, F_SETFL, O_NONBLOCK);
    	x=47;
	}
        //Running the path planner
        pathPlanner->run();
        robotPose = pathPlanner->getRobotPose();
        velocityVector = pathPlanner->getVelocityVector();
        //Filling the velocity vector message
        velocityMsg.header.stamp = ros::Time::now();
         velocityMsg.points.resize(1);
         velocityMsg.points[0].transforms.resize(1);
        velocityMsg.points[0].velocities.resize(1);
        velocityMsg.points[0].transforms[0].translation.x=robotPose.pose.position.x;
        velocityMsg.points[0].transforms[0].translation.y=robotPose.pose.position.y;
	velocityMsg.points[0].transforms[0].translation.z=robotPose.pose.position.z;
       
        velocityMsg.points[0].velocities[0].linear.x=velocityVector(0);
	velocityMsg.points[0].velocities[0].linear.y=velocityVector(1);
	velocityMsg.points[0].velocities[0].linear.z=velocityVector(2);
       
        pub.publish(velocityMsg);
        
       std::ofstream fs2("data_test.txt", std::ofstream::out | std::ofstream::app);
        if (fs2.is_open()){
	fs2 <<  velocityMsg.header.stamp << "\t";
	fs2 << velocityMsg.points[0].transforms[0].translation.x << "\t";
 	fs2 << velocityMsg.points[0].transforms[0].translation.y << "\t";
	fs2 << velocityMsg.points[0].transforms[0].translation.z << "\n";
        }

	x_plot.data=robotPose.pose.position.x;
        y_plot.data=robotPose.pose.position.y;
        z_plot.data=robotPose.pose.position.z;

 	pub_plot_x.publish(x_plot);
	pub_plot_y.publish(y_plot);
	pub_plot_z.publish(z_plot);

        ros::spinOnce();
        rate.sleep();
    }
}

void getParams(ros::NodeHandle &nh) {
    //Path Planner Parameters
    //int surfToBeDef;
    //int surfFlag;
    //int tangFlag;
    bool run_time;
    std::string frame_id;
    //double sigma_multiplier;
    //double xyGain;
    //double zGain;
    //double Kgrad1;
    //double Kgrad2;
    //double Ktang;
    //double dist_sensed_obs;
   // double safety_margin;

    //nh.param("surfFlag", surfFlag,1);
    //nh.param("surfToBeDef", surfToBeDef,1);
    //nh.param("tangFlag",tangFlag,1);
    nh.param("run_time",run_time,true);
    nh.param<std::string>("frame_id",frame_id, "world");
    //nh.param("sigma_multiplier",sigma_multiplier,20.0);
    /*nh.param("xyGain",xyGain,1.0);
    nh.param("zGain",zGain,1.0);
    nh.param("Kgrad1",Kgrad1,0.1);
    nh.param("Kgrad2",Kgrad2,0.1);
    nh.param("Ktang",Ktang,0.1);
    nh.param("dist_sensed_obs",dist_sensed_obs,1.0);*/
    //nh.param("safety_margin",safety_margin,1.0);

    pathPlanner->setFrameId(frame_id);
   // pathPlanner->setSurfFlag(surfFlag);
   // pathPlanner->setSurfToBeDef(surfToBeDef);
   // pathPlanner->setTangFlag(tangFlag);
    pathPlanner->setRunTime(run_time);
    //pathPlanner->setSigmaMultiplier(sigma_multiplier);
   /* pathPlanner->setXYGain(xyGain);
    pathPlanner->setZGain(zGain);
    pathPlanner->setKgrad1(Kgrad1);
    pathPlanner->setKgrad2(Kgrad2);
    pathPlanner->setKtang(Ktang);
    pathPlanner->setDist_sensed_obs(dist_sensed_obs);*/
    //pathPlanner->setSafetyMargin(safety_margin);

    //Parameter to raise the point cloud along z. Used to avoid crashes at testing time.
    // Set to 0 for real implementation
}

void pose_cb (const geometry_msgs::PoseStamped pose_msg) {
    pathPlanner->setRobotPose(pose_msg);
//    if (pose_msg.pose.position.y < -1.5){
//        pathPlanner->setTangFlag(1);
//        //pathPlanner->setSurfFlag(1);
//    }
//    if (pose_msg.pose.position.y > 1.2){
//        pathPlanner->setTangFlag(-1);
//        //pathPlanner->setSurfFlag(-1);
//    }

}


void load_file(){
double sigma_multiplier=20.0;
double safety_margin=1.0;
int surfToBeDef= 1;
int surfFlag = 1;
int tangFlag = 1;
//double sigma_multiplier;
    double xyGain = 1.0;
    double zGain = 1.0;
    double Kgrad1 = 0.1;
    double Kgrad2 = 0.1;
    double Ktang = 0.1;
    double dist_sensed_obs =1.0;
int counter=0;
int counter2=0;

/*    Xo(0) = 1;
    Yo(0) = 0;
    Zo(0) = 0.4;
    Sizeo(0) = 0.2;
    pathPlanner->setObstacleCoordinates(Xo,Yo,Zo,Sizeo);*/
std::string line;
std::ifstream fs1("parameters.txt");

if (fs1.is_open()){
	while (getline(fs1,line))
	{
         counter++;
	}
}
counter--;

fs1.close();
fs1.open("parameters.txt", std::ifstream::in);
if (fs1.is_open()){
fs1 >> sigma_multiplier;
fs1 >> safety_margin;
fs1 >> surfToBeDef;
fs1 >> surfFlag;
fs1 >> tangFlag;
fs1 >> xyGain;
fs1 >> zGain;
fs1 >> Kgrad1;
fs1 >> Kgrad2;
fs1 >> Ktang;
fs1 >> dist_sensed_obs;
pathPlanner->setSigmaMultiplier(sigma_multiplier);
pathPlanner->setSafetyMargin(safety_margin);
pathPlanner->setSurfFlag(surfFlag);
pathPlanner->setSurfToBeDef(surfToBeDef);
pathPlanner->setTangFlag(tangFlag);
pathPlanner->setXYGain(xyGain);
pathPlanner->setZGain(zGain);
pathPlanner->setKgrad1(Kgrad1);
pathPlanner->setKgrad2(Kgrad2);
pathPlanner->setKtang(Ktang);
pathPlanner->setDist_sensed_obs(dist_sensed_obs);

if (counter>0)
    {
    pathPlanner->setHasObstacles(true);
    Eigen::ArrayXd Xo(counter);
    Eigen::ArrayXd Yo(counter);
    Eigen::ArrayXd Zo(counter);
    Eigen::ArrayXd Sizeo(counter);
	while (getline(fs1,line) && counter2<counter)
	{
	fs1 >> Xo(counter2);
	fs1 >> Yo(counter2);
	fs1 >> Zo(counter2);
	fs1 >> Sizeo(counter2);
	counter2++;
	}
     pathPlanner->setObstacleCoordinates(Xo,Yo,Zo,Sizeo);
     }
     else {
          pathPlanner->setHasObstacles(false);
	  } 
     
}

else{
std::cerr << " \n UNABLE TO OPEN THE FILE \n ";
    }
}

